package main

import (
	"context"
	"fmt"
	garage "git.deuxfleurs.fr/garage-sdk/garage-admin-sdk-golang"
	"log"
)

func gadmin() (*garage.APIClient, context.Context) {
	// Set Host and other parameters
	configuration := garage.NewConfiguration()
	configuration.Host = config.S3AdminEndpoint

	// We can now generate a client
	client := garage.NewAPIClient(configuration)

	// Authentication is handled through the context pattern
	ctx := context.WithValue(context.Background(), garage.ContextAccessToken, config.S3AdminToken)
	return client, ctx
}

func grgCreateKey(name string) (*garage.KeyInfo, error) {
	client, ctx := gadmin()

	kr := garage.NewAddKeyRequest()
	kr.SetName(name)
	resp, _, err := client.KeyApi.AddKey(ctx).AddKeyRequest(*kr).Execute()
	if err != nil {
		fmt.Printf("%+v\n", err)
		return nil, err
	}
	return resp, nil
}

func grgGetKey(accessKey string) (*garage.KeyInfo, error) {
	client, ctx := gadmin()

	resp, _, err := client.KeyApi.GetKey(ctx).Id(accessKey).ShowSecretKey("true").Execute()
	if err != nil {
		fmt.Printf("%+v\n", err)
		return nil, err
	}
	return resp, nil
}

func grgSearchKey(name string) (*garage.KeyInfo, error) {
	client, ctx := gadmin()

	resp, _, err := client.KeyApi.GetKey(ctx).Search(name).ShowSecretKey("true").Execute()
	if err != nil {
		fmt.Printf("%+v\n", err)
		return nil, err
	}
	return resp, nil
}

func grgDelKey(accessKey string) error {
	client, ctx := gadmin()

	_, err := client.KeyApi.DeleteKey(ctx).Id(accessKey).Execute()
	if err != nil {
		fmt.Printf("%+v\n", err)
		return err
	}
	return nil
}

func grgCreateBucket(bucket string) (*garage.BucketInfo, error) {
	client, ctx := gadmin()

	br := garage.NewCreateBucketRequest()
	br.SetGlobalAlias(bucket)

	// Create Bucket
	binfo, _, err := client.BucketApi.CreateBucket(ctx).CreateBucketRequest(*br).Execute()
	if err != nil {
		fmt.Printf("%+v\n", err)
		return nil, err
	}
	return binfo, nil
}

func grgAllowKeyOnBucket(bid, gkey string, read, write, owner bool) (*garage.BucketInfo, error) {
	client, ctx := gadmin()

	// Allow user's key
	ar := garage.AllowBucketKeyRequest{
		BucketId:    bid,
		AccessKeyId: gkey,
		Permissions: *garage.NewAllowBucketKeyRequestPermissions(read, write, owner),
	}
	binfo, _, err := client.BucketApi.AllowBucketKey(ctx).AllowBucketKeyRequest(ar).Execute()
	if err != nil {
		fmt.Printf("%+v\n", err)
		return nil, err
	}

	return binfo, nil
}

func allowWebsiteDefault() *garage.UpdateBucketRequestWebsiteAccess {
	wr := garage.NewUpdateBucketRequestWebsiteAccess()
	wr.SetEnabled(true)
	wr.SetIndexDocument("index.html")
	wr.SetErrorDocument("error.html")

	return wr
}

func grgUpdateBucket(bid string, ur *garage.UpdateBucketRequest) (*garage.BucketInfo, error) {
	client, ctx := gadmin()

	binfo, _, err := client.BucketApi.UpdateBucket(ctx).Id(bid).UpdateBucketRequest(*ur).Execute()
	if err != nil {
		fmt.Printf("%+v\n", err)
		return nil, err
	}

	// Return updated binfo
	return binfo, nil
}

func grgAddGlobalAlias(bid, alias string) (*garage.BucketInfo, error) {
	client, ctx := gadmin()

	resp, _, err := client.BucketApi.PutBucketGlobalAlias(ctx).Id(bid).Alias(alias).Execute()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return resp, nil
}

func grgAddLocalAlias(bid, key, alias string) (*garage.BucketInfo, error) {
	client, ctx := gadmin()

	resp, _, err := client.BucketApi.PutBucketLocalAlias(ctx).Id(bid).AccessKeyId(key).Alias(alias).Execute()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return resp, nil
}

func grgDelGlobalAlias(bid, alias string) (*garage.BucketInfo, error) {
	client, ctx := gadmin()

	resp, _, err := client.BucketApi.DeleteBucketGlobalAlias(ctx).Id(bid).Alias(alias).Execute()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return resp, nil
}

func grgDelLocalAlias(bid, key, alias string) (*garage.BucketInfo, error) {
	client, ctx := gadmin()

	resp, _, err := client.BucketApi.DeleteBucketLocalAlias(ctx).Id(bid).AccessKeyId(key).Alias(alias).Execute()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return resp, nil
}

func grgGetBucket(bid string) (*garage.BucketInfo, error) {
	client, ctx := gadmin()

	resp, _, err := client.BucketApi.GetBucketInfo(ctx).Id(bid).Execute()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return resp, nil

}

func grgDeleteBucket(bid string) error {
	client, ctx := gadmin()

	_, err := client.BucketApi.DeleteBucket(ctx).Id(bid).Execute()
	if err != nil {
		log.Println(err)
	}
	return err
}
