{
  description = "A simple LDAP web interface for Bottin";

  inputs.nixpkgs.url =
    "github:nixos/nixpkgs/f758d66c9cc3011f5327f8583908a7803cc019b1";
  inputs.gomod2nix.url =
    "github:tweag/gomod2nix/f95720e89af6165c8c0aa77f180461fe786f3c21";

  outputs = { self, nixpkgs, gomod2nix }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [
          (import "${gomod2nix}/overlay.nix")
        ];
      };
      src = ./.;
      guichet = pkgs.buildGoApplication {
        pname = "guichet";
        version = "0.1.0";
        src = src;
        modules = ./gomod2nix.toml;

        CGO_ENABLED = 0;

        ldflags = [
          "-X main.templatePath=${src + "/templates"}"
          "-X main.staticPath=${src + "/static"}"
        ];

        meta = with pkgs.lib; {
          description = "A simple LDAP web interface for Bottin";
          homepage = "https://git.deuxfleurs.fr/Deuxfleurs/guichet";
          license = licenses.gpl3Plus;
          platforms = platforms.linux;
        };
      };


      container = pkgs.dockerTools.buildImage {
        name = "dxflrs/guichet";
	copyToRoot = pkgs.buildEnv {
          name = "guichet-env";
	  paths = [ guichet pkgs.cacert ];
	};
        config = {
         Entrypoint = "/bin/guichet";
        };
      };
    in {
      packages.x86_64-linux.guichet = guichet;
      packages.x86_64-linux.container = container;
      packages.x86_64-linux.default = guichet;
      
      devShell.x86_64-linux = pkgs.mkShell { nativeBuildInputs = [ pkgs.go pkgs.gomod2nix ]; };
    };
}
