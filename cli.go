package main

import (
	"flag"
	"fmt"
	"golang.org/x/term"
	"os"
	"syscall"
)

var fsCli = flag.NewFlagSet("cli", flag.ContinueOnError)
var passFlag = fsCli.Bool("passwd", false, "Tool to generate a guichet-compatible password hash")

func cliMain(args []string) {
	if err := fsCli.Parse(args); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if *passFlag {
		cliPasswd()
	} else {
		fsCli.PrintDefaults()
		os.Exit(1)
	}
}

func cliPasswd() {
	fmt.Print("Password: ")
	bytepw, err := term.ReadPassword(int(syscall.Stdin))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	pass := string(bytepw)

	hash, err := SSHAEncode(pass)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(hash)
}
