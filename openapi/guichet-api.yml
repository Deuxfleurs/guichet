openapi: 3.0.0
info:
  version: v0.1.0
  title: Guichet API
  description: |
    Programmatic API for managing a user account in Deuxfleurs' Guichet.

    *Disclaimer: this API is currently unstable and does not yet cover all the features availble from Guichet's web interface.*

paths: 
  /website:
    get:
      tags:
        - Website               
      operationId: "ListWebsites"
      summary: "List all websites"
      description: |
        List all the user's websites
      responses:
        '500':
          description: |
            The server can not answer your request because it is in a bad state
        '200':
          description: |
            Returns information about all the user's websites, and user information related to websites.
            The website quota (`quota_website_count`) indicates a quota in number of websites that the user is allowed to create. Website size is set at 100MB by default, and users can increase this quota autonomously up to a certain value defined by `burst_bucket_quota_size`, by default it's set to 200MB but it can be overriden on a per-user basis.
          content:
            application/json:
              schema:
                type: object
                properties:
                  username:
                    type: string
                    example: "user"
                  quota_website_count:
                    $ref: '#/components/schemas/Quota'
                  burst_bucket_quota_size:
                    type: string # TODO: improve?
                    example: "200 Mio"
                  vhosts:
                    type: array
                    items:
                      $ref: '#/components/schemas/Vhost'

  /website/{vhost}:
    parameters:
    - name: vhost
      in: path
      description: "Website identifier"
      required: true
      schema:
        type: string
      style: simple
    get:
      tags: 
        - Website
      operationId: "GetWebsite"
      summary: "Details on a website"
      description: |
        Gets the configuration and status for the website `vhost`.
        This includes information about domains, bucket access keys, and quotas.

        `quota_size` is given in bytes.
        `quota_files` indicates a number of files.
      responses:
        '500':
          description: |
            The server can not answer your request because it is in a bad state
        '404':
          description: |
            Website not found
        '200':
          description: |
            Returns information about the website.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WebsiteInfo'

    post:
      tags:
        - Website
      operationId: "CreateWebsite"
      summary: "Create a new website"
      description: |
        Creates a new website with identifier `vhost`.

      responses:
        '500':
          description: |
            The server can not answer your request because it is in a bad state
        '400':
          description: |
            Your request is malformed, `vhost` must be non-empty.
        '403':
          description: |
            Request forbidden, you have reached your quota of maximum number of websites allowed.
        '200':
          description: |
            Returns information about the website that has been created.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WebsiteInfo'

    patch:
      tags:
        - Website
      operationId: "ConfigureWebsite"
      summary: "Configure website"
      description: |
        Modifies configuration of the website with identifier `vhost`.

      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                quota_size:
                  type: integer
                  format: int64
                  nullable: true
                  example: null
                vhost:
                  type: string
                  nullable: true
                  example: "my-website"
                rotate_key:
                  type: boolean
                  nullable: true
                  example: true

      responses:
        '500':
          description: |
            The server can not answer your request because it is in a bad state
        '400':
          description: |
            Your request is malformed, check your JSON
        '404':
          description: |
            Website not found
        '200':
          description: |
            Returns updated information about the website
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WebsiteInfo'

    delete:
      tags: 
        - Website
      operationId: "DeleteWebsite"
      summary: "Delete Website"
      description: |
        Delete the website with identifier `vhost`.

      responses:
        '500':
          description: |
            The server can not answer your request because it is in a bad state
        '400':
          description: |
            Bad request, empty website identifier or website has unfinished uploads
        '404':
          description: |
            Website not found
        '204':
          description: |
            Success

components:
  securitySchemes:
    basicAuth:
      type: http
      scheme: basic
  schemas:
    WebsiteInfo:
      type: object
      properties:
        vhost:
          $ref: '#/components/schemas/Vhost'
        access_key_id:
          type: string
          example: "GK31c2f218a2e44f485b94239e"
        secret_access_key:
          type: string
          example: "b892c0665f0ada8a4755dae98baa3b133590e11dae3bcc1f9d769d67f16c3835"
        quota_size:
          $ref: '#/components/schemas/Quota'
        quota_files:
          $ref: '#/components/schemas/Quota'
    Vhost:
      type: object
      properties:
        name:
          type: string
          example: "my-website"
        alt_name:
          type: array
          items:
            type: string
            example: "my-other-website-name"
        expanded:
          type: boolean
          example: false
        domain:
          type: string
          example: "my-website.web.deuxfleurs.fr"
    Quota:
      type: object
      properties:
        current:
          type: integer
          format: int64
          example: 1000
        max:
          type: integer
          format: int64
          example: 5000
        ratio:
          type: number
          format: double
          example: 0.2
        burstable:
          type: boolean
          example: true

security:
  - basicAuth: []

servers:
  - description: Guichet Deuxfleurs
    url: https://guichet.deuxfleurs.fr/api/unstable/
