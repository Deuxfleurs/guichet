package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strings"
)

// --- Start page rendering functions
func handleWebsiteList(w http.ResponseWriter, r *http.Request) {
	user := RequireUserHtml(w, r)
	if user == nil {
		return
	}

	ctrl, err := NewWebsiteController(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(ctrl.PrettyList) > 0 {
		http.Redirect(w, r, "/website/inspect/"+ctrl.PrettyList[0], http.StatusFound)
	} else {
		http.Redirect(w, r, "/website/new", http.StatusFound)
	}
}

type WebsiteNewTpl struct {
	Ctrl *WebsiteController
	Err  error
}

func handleWebsiteNew(w http.ResponseWriter, r *http.Request) {
	user := RequireUserHtml(w, r)
	if user == nil {
		return
	}

	ctrl, err := NewWebsiteController(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	tpl := &WebsiteNewTpl{ctrl, nil}

	tWebsiteNew := getTemplate("garage_website_new.html")
	if r.Method == "POST" {
		r.ParseForm()

		bucket := strings.Join(r.Form["bucket"], "")
		if bucket == "" {
			bucket = strings.Join(r.Form["bucket2"], "")
		}

		view, err := ctrl.Create(bucket)
		if err != nil {
			tpl.Err = err
			tWebsiteNew.Execute(w, tpl)
			return
		}

		http.Redirect(w, r, "/website/inspect/"+view.Name.Pretty, http.StatusFound)
		return
	}

	tWebsiteNew.Execute(w, tpl)
}

type WebsiteInspectTpl struct {
	Describe *WebsiteDescribe
	View     *WebsiteView
	Err      error
}

func handleWebsiteInspect(w http.ResponseWriter, r *http.Request) {
	var processErr error

	user := RequireUserHtml(w, r)
	if user == nil {
		return
	}

	ctrl, err := NewWebsiteController(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	bucketName := mux.Vars(r)["bucket"]

	if r.Method == "POST" {
		r.ParseForm()
		action := strings.Join(r.Form["action"], "")
		switch action {
		case "increase_quota":
			_, processErr = ctrl.Patch(bucketName, &WebsitePatch{Size: &user.Quota.WebsiteSizeBursted})
		case "delete_bucket":
			processErr = ctrl.Delete(bucketName)
			if processErr == nil {
				http.Redirect(w, r, "/website", http.StatusFound)
			}
		case "rotate_key":
			do_action := true
			_, processErr = ctrl.Patch(bucketName, &WebsitePatch{RotateKey: &do_action})
		default:
			processErr = fmt.Errorf("Unknown action")
		}

	}

	view, err := ctrl.Inspect(bucketName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	describe, err := ctrl.Describe()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	tpl := &WebsiteInspectTpl{describe, view, processErr}

	tWebsiteInspect := getTemplate("garage_website_inspect.html")
	tWebsiteInspect.Execute(w, &tpl)
}

func handleWebsiteVhost(w http.ResponseWriter, r *http.Request) {
	var processErr error

	user := RequireUserHtml(w, r)
	if user == nil {
		return
	}

	ctrl, err := NewWebsiteController(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	bucketName := mux.Vars(r)["bucket"]

	if r.Method == "POST" {
		r.ParseForm()

		bucket := strings.Join(r.Form["bucket"], "")
		if bucket == "" {
			bucket = strings.Join(r.Form["bucket2"], "")
		}

		view, processErr := ctrl.Patch(bucketName, &WebsitePatch{Vhost: &bucket})
		if processErr == nil {
			http.Redirect(w, r, "/website/inspect/"+view.Name.Pretty, http.StatusFound)
			return
		}
	}

	view, err := ctrl.Inspect(bucketName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	describe, err := ctrl.Describe()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	tpl := &WebsiteInspectTpl{describe, view, processErr}
	tWebsiteEdit := getTemplate("garage_website_edit.html")
	tWebsiteEdit.Execute(w, &tpl)
}
