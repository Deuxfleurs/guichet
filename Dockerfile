FROM golang:alpine3.20 as builder 
RUN apk --update add ca-certificates
COPY . . 
RUN go get -d -v \
    && CGO_ENABLED=0 GOOS=linux go build -a -v -o /guichet


FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /guichet /guichet

ADD static /static
ADD templates /templates

ENTRYPOINT ["/guichet"]
