package main

import (
	"net/http"
	"strings"

	"github.com/go-ldap/ldap/v3"
)

type ProfileTplData struct {
	User           *LoggedUser
	ErrorMessage   string
	Success        bool
	Mail           string
	DisplayName    string
	GivenName      string
	Surname        string
	Visibility     string
	Description    string
	ProfilePicture string
}

func handleProfile(w http.ResponseWriter, r *http.Request) {
	templateProfile := getTemplate("profile.html")

	user := RequireUserHtml(w, r)
	if user == nil {
		return
	}

	data := &ProfileTplData{
		User:         user,
		ErrorMessage: "",
		Success:      false,
	}

	data.Mail = user.Entry.GetAttributeValue("mail")
	data.DisplayName = user.Entry.GetAttributeValue("displayname")
	data.GivenName = user.Entry.GetAttributeValue("givenname")
	data.Surname = user.Entry.GetAttributeValue("sn")
	data.Visibility = user.Entry.GetAttributeValue(FIELD_NAME_DIRECTORY_VISIBILITY)
	data.Description = user.Entry.GetAttributeValue("description")
	data.ProfilePicture = user.Entry.GetAttributeValue(FIELD_NAME_PROFILE_PICTURE)

	if r.Method == "POST" {
		//5MB maximum size files
		r.ParseMultipartForm(5 << 20)

		data.DisplayName = strings.TrimSpace(strings.Join(r.Form["display_name"], ""))
		data.GivenName = strings.TrimSpace(strings.Join(r.Form["given_name"], ""))
		data.Surname = strings.TrimSpace(strings.Join(r.Form["surname"], ""))
		data.Description = strings.Trim(strings.Join(r.Form["description"], ""), "")
		visible := strings.TrimSpace(strings.Join(r.Form["visibility"], ""))
		if visible != "" {
			visible = "on"
		}
		data.Visibility = visible

		profilePicture, err := uploadProfilePicture(w, r, user)
		if err != nil {
			data.ErrorMessage = err.Error()
		}

		if profilePicture != "" {
			data.ProfilePicture = profilePicture
		}

		modify_request := ldap.NewModifyRequest(user.Login.Info.DN(), nil)
		modify_request.Replace("displayname", []string{data.DisplayName})
		modify_request.Replace("givenname", []string{data.GivenName})
		modify_request.Replace("sn", []string{data.Surname})
		modify_request.Replace("description", []string{data.Description})
		modify_request.Replace(FIELD_NAME_DIRECTORY_VISIBILITY, []string{data.Visibility})
		if data.ProfilePicture != "" {
			modify_request.Replace(FIELD_NAME_PROFILE_PICTURE, []string{data.ProfilePicture})
		}

		err = user.Login.conn.Modify(modify_request)
		if err != nil {
			data.ErrorMessage = err.Error()
		} else {
			data.Success = true
		}

	}

	templateProfile.Execute(w, data)
}

type PasswdTplData struct {
	User          *LoggedUser
	ErrorMessage  string
	TooShortError bool
	NoMatchError  bool
	Success       bool
}

func handlePasswd(w http.ResponseWriter, r *http.Request) {
	templatePasswd := getTemplate("passwd.html")

	user := RequireUserHtml(w, r)
	if user == nil {
		return
	}

	data := &PasswdTplData{
		User:         user,
		ErrorMessage: "",
		Success:      false,
	}

	if r.Method == "POST" {
		r.ParseForm()

		password := strings.Join(r.Form["password"], "")
		password2 := strings.Join(r.Form["password2"], "")

		if len(password) < 8 {
			data.TooShortError = true
		} else if password2 != password {
			data.NoMatchError = true
		} else {
			modify_request := ldap.NewModifyRequest(user.Login.Info.DN(), nil)
			pw, err := SSHAEncode(password)
			if err == nil {
				modify_request.Replace("userpassword", []string{pw})
				err := user.Login.conn.Modify(modify_request)
				if err != nil {
					data.ErrorMessage = err.Error()
				} else {
					data.Success = true
				}
			} else {
				data.ErrorMessage = err.Error()
			}
		}
	}

	templatePasswd.Execute(w, data)
}
