package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

func handleAPIWebsiteList(w http.ResponseWriter, r *http.Request) {
	user := RequireUserApi(w, r)

	if user == nil {
		return
	}

	ctrl, err := NewWebsiteController(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if r.Method == http.MethodGet {
		describe, err := ctrl.Describe()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(describe)
		return
	}

	http.Error(w, "This method is not implemented for this endpoint", http.StatusNotImplemented)
	return
}

func handleAPIWebsiteInspect(w http.ResponseWriter, r *http.Request) {
	user := RequireUserApi(w, r)

	if user == nil {
		return
	}

	bucketName := mux.Vars(r)["bucket"]
	ctrl, err := NewWebsiteController(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if r.Method == http.MethodGet {
		view, err := ctrl.Inspect(bucketName)
		if errors.Is(err, ErrWebsiteNotFound) {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		} else if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(view)
		return
	}

	if r.Method == http.MethodPost {
		view, err := ctrl.Create(bucketName)
		if errors.Is(err, ErrEmptyBucketName) {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} else if errors.Is(err, ErrWebsiteQuotaReached) {
			http.Error(w, err.Error(), http.StatusForbidden)
			return
		} else if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusCreated)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(view)
		return
	}

	if r.Method == http.MethodPatch {
		var patch WebsitePatch
		err := json.NewDecoder(r.Body).Decode(&patch)
		if err != nil {
			http.Error(w, errors.Join(fmt.Errorf("Can't parse the request body as a website patch JSON"), err).Error(), http.StatusBadRequest)
			return
		}

		view, err := ctrl.Patch(bucketName, &patch)
		if errors.Is(err, ErrWebsiteNotFound) {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		} else if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(view)
		return
	}

	if r.Method == http.MethodDelete {
		err := ctrl.Delete(bucketName)
		if errors.Is(err, ErrEmptyBucketName) || errors.Is(err, ErrBucketDeleteNotEmpty) || errors.Is(err, ErrBucketDeleteUnfinishedUpload) {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} else if errors.Is(err, ErrWebsiteNotFound) {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		} else if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusNoContent)
		return

	}

	http.Error(w, "This method is not implemented for this endpoint", http.StatusNotImplemented)
	return
}
